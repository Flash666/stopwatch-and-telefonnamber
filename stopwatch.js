/*
Создайте приложение секундомер.
    * Секундомер будет иметь 3 кнопки "Старт, Стоп, Сброс"
    * При нажатии на кнопку стоп фон секундомера должен быть красным, старт - зеленый, сброс - серый
    * Вывод счётчиков в формате ЧЧ:ММ:СС
    * Реализуйте Задание используя синтаксис ES6 и стрелочные функции
*/

const get = id => document.getElementById(id);

let intervalTaiming;
let counterHour = 0;
let counterMinutes = 0;
let counterSeconds = 0;
let flagStart = 0;

const count = () => {
    if (counterSeconds == 60) {
        counterMinutes++;
        counterSeconds = 0;
        if (counterMinutes == 60) {
            counterHour++;
            counterSeconds = 0;
        }
    }
    get("hour").innerHTML = taiming(counterHour);
    get("minutes").innerHTML = taiming(counterMinutes);
    get("seconds").innerHTML = taiming(counterSeconds);
    counterSeconds++;
}

function taiming(tim){
    if (tim < 10) {
        return "0"+tim;
    }
    return tim;
}

get("start").onclick = ()=>{
    if (flagStart == 0){
        get("fon").classList.remove("silver");
        get("fon").classList.remove("green");
        get("fon").classList.remove("black");
        get("fon").classList.add("red");
        intervalTaiming = setInterval(count, 1000);
        flagStart = 1;
    }
}

get("stop").onclick = ()=>{
    get("fon").classList.remove("red");
    get("fon").classList.remove("silver");
    get("fon").classList.remove("black");
    get("fon").classList.add("green");
    clearInterval(intervalTaiming);
    flagStart = 0;
}

get("reset").onclick = ()=>{
    get("fon").classList.remove("red");
    get("fon").classList.remove("green");
    get("fon").classList.remove("black");
    get("fon").classList.add("silver");

    get("hour").innerHTML = "00";
    get("minutes").innerHTML = "00";
    get("seconds").innerHTML = "00";
    counterHour = 0;
    counterMinutes = 0;
    counterSeconds = 0;
}
/*
Реализуйте программу проверки телефона
* Используя JS Создайте поле для ввода телефона и кнопку сохранения
* Пользователь должен ввести номер телефона в формате 000-000-00-00
* Поле того как пользователь нажимает кнопку сохранить проверте правильность ввода номера,
    если номер правильный сделайте зеленый фон и используя
    document.location переведите пользователя на страницу
        https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
    если будет ошибка отобразите её в диве до input.
*/
const telefonNamber = () => {
    pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
    const input = document.getElementById("namber").value;
    if (pattern.test(input)){
        window.location.href = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
    } else {
        document.getElementById("namber").value = "Не венно введён номер!!!";
    }
}